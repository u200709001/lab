package lab9.shapes2d;

public class Square {
    protected double x;


    public Square(){
        x = 1;
    }
    public Square(double x){
        this.x = x;
    }


    public double area(){
        return x*x;
    }

    public String toString(){
        return "Square where \nwidth & height = " + x + "\narea = " + area();
    }
}
