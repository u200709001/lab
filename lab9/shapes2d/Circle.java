package lab9.shapes2d;

public class Circle {
    protected double radius;
    protected static final double PI = 3.14;
    

    public Circle(){
        this.radius = 1;
    }
    public Circle(Double radius){
        this.radius = radius;
    }


    public double area(){
        return PI*radius*radius;
    }

    public String toString(){
        return "Circle where \nradius = " + radius + "\narea = " + area();
    }
}