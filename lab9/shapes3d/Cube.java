package lab9.shapes3d;
import lab9.shapes2d.Square;

public class Cube extends Square{

    public Cube(){
        super();
    }
    public Cube(double x){
        super(x);
    }

    public double area(){
        return super.area() * 6;
    }

    public double volume(){
        return super.area() * x;
    }

    public String toString(){
        return "Cube where\n" + super.toString() + "\narea = "+ area() + "\nvolume = " + volume();
    }
}
