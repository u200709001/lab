package lab9.shapes3d;

import lab9.shapes2d.Circle;

public class Cylinder extends Circle{
    protected double length;
    

    public Cylinder(){
        super();
        length = 1;
    }
    public Cylinder(double radius, double length){
        super(length);
        this.radius = radius;
    }


    public double area(){
        return (2*PI*super.radius * length) + 2*super.area();
    }

    public double volume(){
        return super.area() * length;
    }

    public String toString(){
        return  "Cylinder where \n" + super.toString() + 
                "\nlength = " + length + "\narea = " + area() + "\nvolume = " + volume();
    }
}
