public class Main {
    public static void main(String[] args) {
        // Exercises 1-2-3
        Point pt1 = new Point();
        Rectangle rect1 = new Rectangle(3, 5, pt1);
        
        // Display
        System.out.println( "Width: " + rect1.width + " Height: " + rect1.height + 
                            " Area: " + rect1.area() + " Perimeter: " + rect1.perimeter() + "\nCorners:");
        for( int[] corner : rect1.corners() ) System.out.println("" + corner[0] + ',' + corner[1]);

        // Exercises 4-5
        Point pt2 = new Point(2,5);
        Circle circle1 = new Circle(10, pt1);
        Circle circle2 = new Circle(5, pt2);

        // Display
        System.out.println( "Radius: " + circle1.radius + " Center: " + circle1.center.xCoord + ',' + circle1.center.yCoord +
                            " Area: " + circle1.area() + " Perimeter: " + circle1.perimeter());
        
        System.out.println("Intersect: " + circle1.intersect(circle2));
    }
}
