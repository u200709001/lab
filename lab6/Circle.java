public class Circle {
    public int radius;
    public Point center;

    private static double PI = 3.14;

    public Circle(int r, Point center){
        this.radius = r;
        this.center = center;
    }

    public double area(){
        return PI*radius*radius;
    }

    public double perimeter(){
        return 2*PI*radius;
    }

    public boolean intersect(Circle other){
        double distance =   Math.sqrt(
                                Math.pow( (this.center.xCoord - other.center.xCoord), 2) + 
                                Math.pow( (this.center.yCoord - other.center.yCoord), 2) 
                            );
        
        return this.radius + other.radius >= distance;
    }
    
}
