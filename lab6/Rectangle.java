public class Rectangle {
    public int width;
    public int height;
    private Point topLeft;

    public Rectangle(int width, int height, Point topLeft){
        this.width = width;
        this.height = height;
        this.topLeft = topLeft;
    }

    public int area(){
        return this.width * this.height;
    }

    public int perimeter(){
        return (this.width + this.height) * 2;
    }

    public int[][] corners(){
        int x = this.topLeft.xCoord;
        int y = this.topLeft.yCoord;
        int[][] result = { {x, y} , {x+width,y}, {x+width, y+height}, {x, y+height} };
        return result;
    }
}
