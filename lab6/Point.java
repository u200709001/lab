public class Point{

    public int xCoord;
    public int yCoord;

    public Point(){
        this.xCoord = 0;
        this.yCoord = 0;
    }
    public Point(int x, int y){
        this.xCoord = x;
        this.yCoord = y;
    }
}