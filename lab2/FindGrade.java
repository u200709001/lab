public class FindGrade {
    /*
    * 90 <= score <= 100 : A
    * 80 <= score < 90   : B
    * 70 <= score < 80   : C
    * 60 <= score < 70   : D
    * 0  <= score < 60   : F
    */
    public static void main(String[] args) {
        byte score = (byte) Integer.parseInt(args[0]);
        
        System.out.println(EvaluateScore(score));
    }

    public static String EvaluateScore(byte score){
        if (score >= 90)        return "Your grade is A";
        else if (score >= 80)   return "Your grade is B";
        else if (score >= 70)   return "Your grade is C";
        else if (score >= 60)   return "Your grade is D";
        else                    return  "Your grade is F";
    } 
}
