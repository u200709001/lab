public class MyDate {
    public int day;
    public int month;
    public int year;

    // Keeping the first element 0 so that we can pass month as index for maxDays
    private int[] maxDays = {0,31,28,31,30,31,30,31,31,30,31,30,31};

    public MyDate(){
        this(1,1,2000);
    }
    public MyDate(int day, int month, int year){
        this.day = day;
        this.month = month;
        this.year = year;
        updateMaxDays();
    }




    // Increment Methods
    public void incrementDay(){
        incrementDay(1);
    }
    public void incrementDay(int x){

        for(int i=0; i<x; i++){
            day++;

            // End of month
            if (day > maxDays[month]){
                day = 1;
                incrementMonth();
            } 
        }
    }
    
    public void incrementMonth(){
        incrementMonth(1);
    }
    public void incrementMonth(int x){
        int lastDay = day;
        
        for(int i=0; i<x; i++){
            month++;

            // Month Handler
            if (month > 12){
                month = 1;
                incrementYear();
            }

            // Day Handler
            if (lastDay >= maxDays[month]){
                day = maxDays[month];
            }
        }
        
    }

    public void incrementYear(){
        incrementYear(1);
    }
    public void incrementYear(int x){

        for(int i=0; i<x; i++){

            year++;
            updateMaxDays();
            
            // Update day for feb leap year
            // TODO: Remove controlling for every iteration
            if (month == 2 && day > 27){
                day = maxDays[month];
            }

        }
    }




    // Decrement Methods
    public void decrementDay(){
        decrementDay(1);
    }
    public void decrementDay(int x){

        for(int i=0; i<x; i++){

            day--;

            // End of the month
            if (day < 1){
                decrementMonth();
                day = maxDays[month];
            }

        }

    }

    public void decrementMonth(){
        decrementMonth(1);
    }
    public void decrementMonth(int x){
        int lastDay = day;
        
        for(int i=0; i<x; i++){
            month--;

            // Month Handler
            if (month < 1){
                month = 12;
                decrementYear();
            }

            // Day Handler
            if (lastDay >= maxDays[month]){
                day = maxDays[month];
            }
        }
    }

    public void decrementYear(){
        decrementYear(1);
    }
    public void decrementYear(int x){
        for(int i=0; i<x; i++){

            year--;
            updateMaxDays();
            
            // Update day for feb leap year
            // TODO: Remove controlling for every iteration
            if (month == 2 && day > 27){
                day = maxDays[month];
            }

        }
    }




    // Other Methods
    public boolean isBefore(MyDate other){
        if (other.year < this.year) return false;
        if (other.month < this.month) return false;
        if (other.day < this.month) return false;
        return true;
    }
    
    public boolean isAfter(MyDate other){
        return !this.isBefore(other);
    }
    
    public int dayDifference(MyDate other){
        int dayDiff = 0;
        MyDate copy = new MyDate(day, month, year);
        
        if (isBefore(other)){
            while(copy.isBefore(other)){
                dayDiff++;
                copy.incrementDay();
            }
        } else{
            while(copy.isAfter(other)){
                dayDiff++;
                copy.decrementDay();
            }
        }
        
        return dayDiff;
    }
    



    // Utility Methods
    private void updateMaxDays(){
        maxDays[2] = year % 4 == 0 ? 29 : 28;
    }




    // Display Methods
    public String toString(){
        String day = this.day > 9 ? ""+this.day : "0"+this.day;
        String month = this.month > 9 ? ""+this.month : "0"+this.month;

        return ""+year+"-"+month+"-"+day;
    }
}
