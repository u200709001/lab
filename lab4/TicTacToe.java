import java.io.IOException;
import java.util.Scanner;

public class TicTacToe {

	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in	);
		int row, col, movesLeft = 9;
		char[][] board = { { ' ', ' ', ' ' }, { ' ', ' ', ' ' }, { ' ', ' ', ' ' } };

		// Main Loop
		while(true){
			// Interrupt
			if( movesLeft == 0 ){
				printBoard(board);
				System.out.println("Draw!");
				break;
			}

			// Player 1
			while(true){
				// Inputs
				while(true){
					// Display
					printBoard(board);
					System.out.print("Player 1 enter row number:");
					row = reader.nextInt();
					System.out.print("Player 1 enter column number:");
					col = reader.nextInt();
					
					if(row > 0 && row < 4 && col > 0 && col < 4){
						break;
					}

					System.out.println("Location is out of range, please enter another coordinate.");
				}
				
				// Sign
				if(board[row-1][col-1] == ' '){
					board[row - 1][col - 1] = 'X';
					break;
				}
				
				// Error
				System.out.println("Given location is already occupied, please enter another coordinate.");
			}
			movesLeft--;

			// Check for Player 1
			if( checkBoard(board) ){
				printBoard(board);
				System.out.println("Player 1 Won!");
				break;
			}

			// Player 2
			while(true){
				// Inputs
				while(true){
					// Display
					printBoard(board);
					System.out.print("Player 2 enter row number:");
					row = reader.nextInt();
					System.out.print("Player 2 enter column number:");
					col = reader.nextInt();
					
					if(row > 0 && row < 4 && col > 0 && col < 4){
						break;
					}

					System.out.println("Location is out of range, please enter another coordinate.");
				}
				
				// Sign
				if(board[row-1][col-1] == ' '){
					board[row - 1][col - 1] = 'O';
					break;
				}

				// Error
				System.out.println("Given location is already occupied, please enter another coordinate.");
			}
			movesLeft--;

			// Check for Player 2
			if( checkBoard(board) ){
				printBoard(board);
				System.out.println("Player 2 Won!");
				break;
			}
		}

		reader.close();
	}

	public static void printBoard(char[][] board) {
		System.out.println("    1   2   3");
		System.out.println("   -----------");
		for (int row = 0; row < 3; ++row) {
			System.out.print(row + 1 + " ");
			for (int col = 0; col < 3; ++col) {
				System.out.print("|");
				System.out.print(" " + board[row][col] + " ");
				if (col == 2)
					System.out.print("|");

			}
			System.out.println();
			System.out.println("   -----------");

		}

	}

	public static boolean checkBoard(char[][] board) {
		// Win states: Horizontal 3, Vertical 3, Cross

		// Cross
		if( board[0][0] != ' ' && board[0][0] == board[1][1] && board[0][0] == board[2][2] ) return true;
		if( board[0][2] != ' ' && board[0][2] == board[1][1] && board[0][2] == board[2][0] ) return true;

		for(int i=0; i < board.length; i++){
			// We'll use "i" as multifunctional since board row and col has same amount
			
			// Vertical
			if( board[i][0] != ' ' && board[i][0] == board[i][1] && board[i][0] == board[i][2] ) return true;
			
			// Horizontal
			if( board[0][i] != ' ' && board[0][i] == board[1][i] && board[0][i] ==  board[2][i] ) return true;
		}
		
		return false;
	}
}