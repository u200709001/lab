package stack;

import java.util.ArrayList;

public class StackArrayImpl implements Stack {
    private ArrayList<StackItem> stack = new ArrayList<StackItem>();

    @Override
    public void push(Object item) {
        StackItem newItem = new StackItem(item);
        stack.add(newItem);
    }

    @Override
    public Object pop() {
        int lastIndex = stack.size()-1;
        StackItem old = stack.get(lastIndex);
        stack.remove(lastIndex);
        
        return old.getItem();
    }

    @Override
    public boolean empty() {
        return stack.isEmpty();
    }

    
}
