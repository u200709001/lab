package stack;

public class StackImpl implements Stack{
    private StackItem top;

    @Override
    public void push(Object item) {
        StackItem newItem = new StackItem(item);
        newItem.setNext(top);
        top = newItem;
    }

    @Override
    public Object pop() {
        Object old = top.getItem();
        top = top.getNext();
        return old;
    }

    @Override
    public boolean empty() {
        return top == null;
    }
}
