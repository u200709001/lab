package shapes;

public class Square extends Shape{
    private double height;

    public Square(double height){
        super();
        this.height = height;
    }

    public double area(){
        return height*height;
    }
}
