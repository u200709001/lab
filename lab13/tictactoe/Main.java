package tictactoe;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws InvalidMoveException {
        Scanner reader = new Scanner(System.in);

        Board board = new Board();

        System.out.println(board);
        while (!board.isEnded()) {

            int player = board.getCurrentPlayer();
            int row = 0, col = 0;

            Boolean isValid = false;
            while(!isValid){
                System.out.print("Player " + player + " enter row number:");
                try{
                    row = Integer.valueOf(reader.nextLine());
                    System.out.println("Row : " + row);
                    isValid = true;
                } catch (NumberFormatException ex){
                    System.out.println("Invalid row, please try again.");
                }
            }

            isValid = false;
            while(!isValid){
                System.out.print("Player " + player + " enter column number:");
                try{
                    col = Integer.valueOf(reader.nextLine());
                    System.out.println("Column : " + col);
                    isValid = true;
                } catch (NumberFormatException ex){
                    System.out.println("Invalid column, please try again.");
                }
            }

            try{
                board.move(row, col);
            } catch (InvalidMoveException ex){
                System.out.println(ex.getMessage());
                continue;
            }
            System.out.println(board);
        }


        reader.close();
    }


}
