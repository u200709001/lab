public class GCDRec {
    public static void main(String[] args) {
        System.out.println(
            GCD( Integer.parseInt(args[0]), Integer.parseInt(args[1]) )
        );
    }

    public static int GCD(int a, int b){
        int q,r;

        q = a / b; 
        r = a - (q*b);
        if (r==0) return b;

        return GCD(b,r);
    }
}
