public class FindPrimes {
    public static void main(String[] args) {
        // Recalling one of my past repositories https://github.com/younver/Algorithms/blob/main/sieve_of_eratosthenes.cpp
        // I'm in love with interesting algorithms :3

        // Initialization
        boolean[] primes = new boolean[300];
        int n = Integer.parseInt(args[0]);

        // Sieve of Eratosthenes - The Most Optimized Way to Find Primes
        // Unsigned ones are primes
        for(int i=2; i*i < n; i++){
            // if that number not signed
            if(!primes[i]){
                // from square of i to n, increment by i, sign 'em all 
                for(int j = i*i; j<n; j+=i){
                    primes[j] = true;
                }
            }
        }

        // Display
        for(int i=2; i<n; i++){
            if(!primes[i]){
                System.out.print("," + i);
            }
        }

    }
}
