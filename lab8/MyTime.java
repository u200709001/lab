public class MyTime {
    int hour;
    int minute;
    int dayDiff;
    int hourDiff;

    // Constructors
    public MyTime(){
        this(0, 0);
    }
    public MyTime(int hour, int minute){
        this.hour = hour;
        this.minute = minute;
    }


    // Methods
    public void incrementHour(){
        incrementHour(1);
    }
    public void incrementHour(int x){
        dayDiff = 0;

        for(int i=0; i<x; i++){
            hour++;
        
            // Next Day
            if (hour > 23){
                hour = 0;
                dayDiff++;
            }
        }
    }

    public void incrementMinute(){
        incrementMinute(1);
    }
    public void incrementMinute(int x){
        hourDiff = 0;

        for(int i=0; i<x; i++){
            minute++;

            // Next Hour
            if (minute > 59){
                minute = 0;
                hourDiff++;
            }
        }
    }

    public void decrementHour(){
        decrementHour(1);
    }
    public void decrementHour(int x){
        dayDiff = 0;

        for(int i=0; i<x; i++){
            hour--;

            // Previous Day
            if (hour < 0){
                hour = 23;
                dayDiff++;
            }
        }
    }

    public void decrementMinute(){
        decrementMinute(1);
    }
    public void decrementMinute(int x){
        hourDiff = 0;

        for(int i=0; i<x; i++){
            minute--;

            // Previous Hour
            if (minute < 0){
                minute = 59;
                hourDiff++;
            }
        }
    }


    // Other
    public int minuteDifference(MyTime other){
        int minuteDiff = 0;

        MyTime copy = new MyTime(hour, minute);

        if (isBefore(other)){
            while (copy.isBefore(other)){
                copy.incrementMinute();
                minuteDiff++;
            }
        }
        else if (isAfter(other)){
            while (copy.isAfter(other)){
                decrementMinute();
                minuteDiff++;
            }
        }

        return minuteDiff;
    }

    public boolean isBefore(MyTime other){
        int x = Integer.parseInt(this.toString().replace(":", ""));
        int y = Integer.parseInt(other.toString().replace(":", ""));
        
        return x<y;
    }
    public boolean isAfter(MyTime other){
        return !isBefore(other);
    }
    
    public String toString(){
        return (hour < 10 ? "0"+hour : ""+hour) + ":" + (minute < 10 ? "0"+minute : ""+minute);
    }
}
