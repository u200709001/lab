public class MyDateTime {
    MyDate date;
    MyTime time;


    // Constructors
    public MyDateTime(MyDate date, MyTime time){
        this.date = date;
        this.time = time;
    }
    

    // Date Methods
    public void incrementDay(){
        date.incrementDay();
    }
    public void incrementDay(int x){
        date.incrementDay(x);
    }

    public void incrementMonth(){
        date.incrementMonth();
    }
    public void incrementMonth(int x){
        date.incrementMonth(x);
    }

    public void incrementYear(){
        date.incrementYear();
    }
    public void incrementYear(int x){
        date.incrementYear(x);
    }

    public void decrementDay(){
        date.decrementDay();
    }
    public void decrementDay(int x){
        date.decrementDay(x);
    }

    public void decrementMonth(){
        date.decrementMonth();
    }
    public void decrementMonth(int x){
        date.decrementMonth(x);
    }

    public void decrementYear(){
        date.decrementYear();
    }
    public void decrementYear(int x){
        date.decrementYear(x);
    }

    
    // Time Methods
    public void incrementHour(){
        incrementHour(1);
    }
    public void incrementHour(int x){
        time.incrementHour(x);

        // Day Handler
        date.incrementDay(time.dayDiff);
    }

    public void incrementMinute(){
        incrementMinute(1);
    }
    public void incrementMinute(int x){
        time.incrementMinute(x);

        // Hour Handler
        incrementHour(time.hourDiff);
    }

    public void decrementHour(){
        decrementHour(1);
    }
    public void decrementHour(int x){
        time.decrementHour(x);
        
        // Day Handler
        date.decrementDay(time.dayDiff);
    }

    public void decrementMinute(){
        decrementMinute(1);
    }
    public void decrementMinute(int x){
        time.decrementMinute(x);

        // Hour Handler
        decrementHour(time.hourDiff);
    }


    // Other
    public String dayTimeDifference(MyDateTime other){
        int minuteDiff = 0;

        MyDateTime copy = new MyDateTime(date, time);

        if (isBefore(other)){
            while (copy.isBefore(other)){
                copy.incrementMinute();
                minuteDiff++;
            }
        }
        else if (isAfter(other)){
            while (copy.isAfter(other)){
                decrementMinute();
                minuteDiff++;
            }
        }

        // Format
        int dayDiff = minuteDiff / 1440;
        minuteDiff = minuteDiff % 1440;
        int hourDiff = minuteDiff / 60;
        minuteDiff = minuteDiff % 60;

        return  (dayDiff == 0 ? "" : dayDiff+" day(s) ") + (hourDiff == 0 ? "" : hourDiff+" hour(s) ") + 
                (minuteDiff == 0 ? "" : minuteDiff+" minute(s).");
    }

    public boolean isBefore(MyDateTime other){
        if (date.isBefore(other.date)) return true;
        else if (time.isBefore(other.time)) return true;
        return false;
    }
    public boolean isAfter(MyDateTime other){
        return !isBefore(other);
    }

    public String toString(){
        return date.toString() + " " + time.toString();
    }
}
